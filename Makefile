all: build

npm:
	npm run build

start:
	npm run start

copy: 
	cp build ../kafkonsole-server/ui -r

build: npm copy