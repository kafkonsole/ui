/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import React, { useRef, useCallback } from 'react';
import { useRouteMatch } from 'react-router-dom';
import Typography from '@material-ui/core/Typography';
import RenderStore from '../store/RenderStore';
import { inject } from 'mobx-react';

interface RootTopInfoProps {
    renderStore?: RenderStore
}

function RootTopInfo({ renderStore }: RootTopInfoProps) {
    const root = useRouteMatch("/");
    const measuredRef = useCallback((node: HTMLDivElement) => {
        if (renderStore) {
            renderStore.references.appBarRef = node
        }
    }, [renderStore]);

    if (root?.isExact) {
        return <Typography variant="h6" noWrap>
            Kafkonsole> {/* Add basic stats here */}
        </Typography>
    }

    if (!renderStore) {
        return <></>
    }
    return <div ref={measuredRef} />
    // renderStore.references.appBarRef = appBarRef

    // return <div ref={appBarRef}>
    // </div>
}

export default inject('renderStore')(RootTopInfo)