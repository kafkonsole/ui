/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import React, { useEffect, useState } from 'react';
import { useRouteMatch } from 'react-router';
import { inject, observer } from 'mobx-react';
import RenderStore from '../store/RenderStore';
import { Portal } from '@material-ui/core';
import InfiniteConsumer from './InfiniteConsumer';
import Expansion from './Expansion'
import Producer from './Producer';

type MatchParams = { [key: string]: string }

interface TopicViewProps {
    renderStore?: RenderStore
}

function TopicView({ renderStore }: TopicViewProps) {
    const match = useRouteMatch<MatchParams>("/topics/:topic");
    if (!match?.isExact) {
        return <></>
    }
    const topic = match.params["topic"]
    const appBarContainer = renderStore?.references.appBarRef

    return <>
        <Portal container={appBarContainer}>
            {topic}
            {/* TODO: add topic stats */}
        </Portal>
        <Expansion panels={[
            {
                name: "producer",
                title: "Producer",
                details: (<Producer />)
            },
            {
                name: "infinite-consumer",
                title: "Infinite Consumer",
                details: (<InfiniteConsumer />)
            },
        ]} />
        {/* <InfiniteConsumer /> */}
    </>
}

export default inject('renderStore')(observer(TopicView))