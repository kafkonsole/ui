/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import InfiniteScroll from 'react-infinite-scroller';
import React, { useState, MutableRefObject, useEffect, SyntheticEvent, ChangeEvent } from 'react'
import { Portal, TextField, Button, Tooltip, Grid } from '@material-ui/core';
import { useRouteMatch } from 'react-router-dom';
import ConsumersStore from '../store/ConsumersStore';
import { observer, inject } from 'mobx-react'
import TopicsStore from '../store/TopicsStore';
import ClientStore from '../store/ClientStore';
import LastNRecords from './LastNRecords';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

interface ProduceringRecord {
    value: ArrayBuffer
}

interface Props {
    topicsStore?: TopicsStore
    clientStore?: ClientStore
}

type MatchParams = { [key: string]: string }

function str2ab(str: string) {
    // var buf = new ArrayBuffer(str.length * 2); // 2 bytes for each char
    // var bufView = new Uint16Array(buf);
    // for (var i = 0, strLen = str.length; i < strLen; i++) {
    //     bufView[i] = str.charCodeAt(i);
    // }
    // return buf;
    var enc = new TextEncoder()
    return enc.encode(str)
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        padTop: {
            paddingTop: theme.spacing(0.3),
        },
    }))

function Producer({ topicsStore, clientStore }: Props) {
    const classes = useStyles();
    const match = useRouteMatch<MatchParams>("/topics/:topic");
    const client = clientStore?.client
    const topicName = match?.params["topic"]
    const [textValue, setTextValue] = useState("")
    const clientProduce = client?.produce
    if (!topicName || !topicsStore || !match?.isExact || !clientProduce) {
        return <></>
    }
    const produce = () => clientProduce(topicName, str2ab(textValue))

    const topic = topicsStore.topics.get(topicName)
    if (!topic) {
        console.error("no topic!")
    }
    // const submit = (e: SyntheticEvent) => {
    //     e.preventDefault();
    //     produce()
    // }

    const change = (ev: React.FormEvent<HTMLElement>) => {
        const actualTarget = ev.currentTarget as HTMLInputElement
        setTextValue(actualTarget.value)
    }

    const keydown = (e: React.KeyboardEvent) => {
        if (e.ctrlKey && e.keyCode === 13) {
            produce()
        }
    }

    return <Grid container>
        <Grid item xs={12}>
            <TextField
                multiline
                fullWidth
                onKeyDown={keydown}
                onChange={change}
                id="producing-value-text-field"
                label="Value Text" />
            <Tooltip title={"Hotkey: Cntrl+Enter"}>
                <Button color="primary" onClick={produce} >Produce</Button>
            </Tooltip>
        </Grid>
        <Grid item xs={12} className={classes.padTop}>
            {topic && <LastNRecords topic={topic} n={10} />}
        </Grid>
    </Grid>
}

export default inject('topicsStore', 'clientStore')(observer(Producer))
