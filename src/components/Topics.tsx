/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import React, { useEffect, useState } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { values } from 'mobx';
import TopicsStore, { Topic } from '../store/TopicsStore';
import { inject, observer } from 'mobx-react';
import KafkonsoleClient from '../api/KafkonsoleClient';
import ClientStore from '../store/ClientStore';
import { TextField, Button, Paper, Toolbar } from '@material-ui/core';
import TopicView from './TopicView';
import TestingMarkers from '../api/TestingMarkers';
import MaterialTable, { Column, MTableAction } from 'material-table'

const columns: Column<Topic>[] = [
    {
        title: 'Topic',
        field: 'name',
        render: topic => <Link to={`topics/${topic.name}`}>{topic.name}</Link>
    }
]

interface TopicsProps {
    topicsStore?: TopicsStore
    clientStore?: ClientStore
}

function Topics({ topicsStore, clientStore }: TopicsProps) {
    const topics = useRouteMatch("/topics");
    const load = clientStore?.client?.loadTopics
    const deletes = clientStore?.client?.deleteTopics
    const create = clientStore?.client?.createTopic
    useEffect(() => { // initial topics load
        if (load) {
            console.log("Loading topics")
            load()
        } else {
            console.warn("No loadTopics callback")
        }
    }, [load])
    const [newTopicName, setNewTopicName] = useState("");

    if (!topics?.isExact) {
        return <TopicView />
    }
    if (!topicsStore) {
        return <></>
    }

    const handleNewTopicNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setNewTopicName(event.target.value);
    }
    const createNewTopic = () => {
        create ? create(newTopicName) : console.warn("no create callback")
        // load ? setTimeout(load, 5000) : console.warn("no load callback")
        load ? load() : console.warn("no load callback")
    }

    const items = Array.from(topicsStore.topics.values())

    const onDelete = (evt: any, data: Topic | Topic[]) => {
        var names: string[] | undefined
        if (data instanceof Array) {
            names = data.map(topic => topic.name)
        } else {
            names = [data.name]
        }
        deletes ? deletes(...names) : console.warn("no delete callback")
        load ? load() : console.warn("no load callback")

    }

    return <>
        <div  {...TestingMarkers.topicsTable}>
            <MaterialTable

                columns={columns}
                data={items}
                title="Topics"
                options={{
                    selection: true
                }}
                actions={[
                    {
                        tooltip: 'Delete All Selected Topics',
                        icon: 'delete',
                        onClick: onDelete
                    }
                ]}
                components={{
                    Action: props => {
                        if (props?.action?.icon === 'delete') {
                            var marker = TestingMarkers.topicsDelete
                            return (
                                <div {...marker}>
                                    <MTableAction {...props} />
                                </div>
                            )
                        }
                        return <MTableAction {...props} />
                    }

                }}
            />
        </div>

        <Paper>
            <Toolbar>
                <Button {...TestingMarkers.createTopic} variant="contained" color="primary" onClick={createNewTopic}>Create</Button>
                <TextField {...TestingMarkers.newTopicName} onChange={handleNewTopicNameChange} label="New topic" variant="outlined" />
            </Toolbar>
        </Paper>
    </>
}

export default inject('topicsStore', 'clientStore')(observer(Topics));
