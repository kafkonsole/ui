/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import InfiniteScroll from 'react-infinite-scroller';
import React, { useState, MutableRefObject, useEffect } from 'react'
import { Portal } from '@material-ui/core';
import { useRouteMatch } from 'react-router-dom';
import ConsumersStore from '../store/ConsumersStore';
import { observer, inject } from 'mobx-react'
import TopicsStore from '../store/TopicsStore';
import ClientStore from '../store/ClientStore';
import DisplayRecord from './DisplayRecord';

interface DisplayedRecord {
    offset: number
}

interface Props {
    topicsStore?: TopicsStore
    clientStore?: ClientStore
}

type MatchParams = { [key: string]: string }

function InfiniteConsumer({ topicsStore, clientStore }: Props) {
    const match = useRouteMatch<MatchParams>("/topics/:topic");
    const client = clientStore?.client
    const consume = client?.consume
    const subscribe = client?.subscribe
    const topicName = match?.params["topic"]

    useEffect(() => {
        topicName
            ? subscribe
                ? subscribe(topicName)
                : console.warn("no subscribe callback")
            : console.warn("no topic name")
    }, [topicName, client])

    if (!topicName || !topicsStore || !match?.isExact || !clientStore?.client) {
        return <></>
    }
    console.log("passed checks...")
    const topic = topicsStore.topics.get(topicName)
    if (!topic) {
        console.error("no topic!")
    }
    if (!topic?.subscription) {
        return <></>
    }

    const records = topic.subscription.records
    const sid = topic?.subscription?.id
    console.log("rendering infinite scroll")
    return <>
        <InfiniteScroll
            pageStart={0}
            loadMore={() => {
                console.log("consuming more...")
                consume ? consume(sid) : console.warn("no consume callback")
                // records.push({ offset: 10, value: "" }) 
            }}
            hasMore={true || false}
            loader={
                < div className="loader" key={0} > {
                    'Waiting for more...'
                }</div >
            }
        >
            {
                records && records.map(record => <DisplayRecord {...record}></DisplayRecord>) || <div />
            }

        </InfiniteScroll ></>
}

export default inject('topicsStore', 'clientStore')(observer(InfiniteConsumer))

// interface DisplayedRecord {
//     offset: number
// }

// interface Props {
//     consumersStore?: ConsumersStore
// }

// type MatchParams = { [key: string]: string }

// function InfiniteConsumer({ consumersStore }: Props) {
//     const match = useRouteMatch<MatchParams>("/topics/:topic");
//     if (!consumersStore || !match?.isExact) {
//         return <></>
//     }
//     const topic = match.params["topic"]
//     const consumer = consumersStore.consumers[topic]
//     if (!consumer) {
//         return <></>
//     }

//     const records = consumer.consumed

//     return <>
//         <InfiniteScroll
//             pageStart={0}
//             loadMore={() => {
//                 // records.push({ offset: 10, value: "" }) 
//             }}
//             hasMore={true || false}
//             loader={
//                 < div className="loader" key={0} > {
//                     'Waiting for more...'
//                 }</div >
//             }
//         >
//             {
//                 records.map(record => <div>
//                     {record.offset}: {record.value}
//                 </div>)
//             }

//         </InfiniteScroll ></>
// }

// export default inject('consumersStore')(observer(InfiniteConsumer))