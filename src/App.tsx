/**
 * Copyright (c) 2019 Timur Sultanaev
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

import React from 'react';
import Root from './Root'
import {
  HashRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Provider } from 'mobx-react';
import ConsumersStore from './store/ConsumersStore';
import TopicsStore from './store/TopicsStore';
import ClientStore from './store/ClientStore';
import RenderStore from './store/RenderStore';

const clientStore = new ClientStore();
(document as any)._clientStore = clientStore;

const portEnv = process.env.REACT_APP_KAFKONSOLE_SERVER_PORT
const port = portEnv ? parseInt(portEnv) : undefined

clientStore.config = {
  hostname: process.env.REACT_APP_KAFKONSOLE_SERVER_HOST,
  port: port,
}

const consumersStore = new ConsumersStore();
(document as any)._consumersStore = consumersStore;

const topicsStore = new TopicsStore();
(document as any)._topicsStore = topicsStore;

const renderStore = new RenderStore();

declare var Go: any;
async function init() {
  const res = await WebAssembly.compileStreaming(fetch('./main.wasm'))
  const gorunner = new Go();
  var inst = await WebAssembly.instantiate(res, gorunner.importObject);
  gorunner.run(inst)
}

init();

export default function App() {
  return <Router>
    <Provider
      topicsStore={topicsStore}
      consumersStore={consumersStore}
      clientStore={clientStore}
      renderStore={renderStore}
    >
      <Root />
    </Provider>
  </Router >
}
